#!/bin/bash

# Empresa: INFOWAY
# Data criação: 09/10/2017
# Autor: Álvaro Bacelar
# email: alvaro@alvarobacelar.com

# Paths dos comandos
BASE=$1
BUCKET=$2
SRVHOST=$3
SRVPATH=$4
SRVPORT=$5
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
MYSQLDUMPOPTS="--opt"
CHOWN="$(which chown)"
CHMOD="$(which chmod)"
TAR="$(which tar)"
S3CMD="$(which aws)"
DU="$(which du)"
DEST="/mnt/backupsdodia"
DESTBASE="$DEST/$BASE"
NOW="$(date +"%Y%m%d-%H%M")"
FILE="Backup-$BASE-$NOW"
FILETAR="$FILE.tar.gz"
FILEDUMP="$FILE.backup"


backupBase(){
  if [ ! -e "$DESTBASE" ]; then
    echo "$(date) - Criando diretorio $DESTBASE"
    mkdir -p $DESTBASE
  fi
  if [ $? -ne 0 ] ; then
    echo "$(date) - Erro ao criar o diretorio $DESTBASE"
    exit 1
  fi
  echo "$(date) - Entrando no diretorio para iniciar o backup"
  cd $DESTBASE

  if [ -z $BASE ] ; then
    echo "$(date) - Falha ao executar o backup, não foi informado a base a ser realizado o backup"
    exit 1
  fi
  echo "$(date) - Testando a conexão com a base"
  echo
  $MYSQL -Bse 'show databases'
  if [ $? -ne 0 ] ; then
    echo "$(date) - Erro ao testar a conexão com a base"
    exit 1
  fi
  # Dump
  echo "$(date) - Iniciando o dump da base de dados $BASE"
  $MYSQLDUMP $MYSQLDUMPOPTS $BASE > $FILEDUMP
  echo "Arquivo de dump: "
  $DU -sh $FILEDUMP
  # Manda email caso nao consiga realizar dump
  if [ $? -ne 0 ] ; then
    echo "$(date) - Falha ao realizar o dump da base $BASE"
    rm -rf $FILEDUMP
    exit 1
  fi
  echo "$(date) - Finalizado o dump da base $BASE"
  # Compactacao
  echo "$(date) - Iniciando a compactação do arquivo $FILEDUMP"
  $TAR czf $FILETAR $FILEDUMP
  echo "Arquivo compactado:"
  $DU -sh $FILETAR
  # Saída com erro caso haja falha na compactação
  if [ $? -ne 0 ] ; then
    echo "$(date) - Falha ao compactar o arquivo $FILEDUMP"
    rm -rvf $FILETAR
    exit 1
  fi
  echo "$(date) - Finalizado a compactação, removendo arquivo"
  rm -rvf $FILEDUMP
  # verifica se existe o segundo parametro para enviar para o bucket remoto
  if [ -z $BUCKET ]; then
    echo "$(date) - Backup da base $BASE finalizado com sucesso!"
  else
    sendFileS3
  fi

}

sendFileHost(){
  echo "$(date) - Enviando o arquivo $FILETAR para o host $SRVHOST:$SRVPATH"
  scp -P $SRVPORT $FILETAR root@$SRVHOST:$SRVPATH
  if [ $? -ne 0 ] ; then
    echo "$(date) - Falha ao enviar o arquivo de backup para o host $SRVHOST"
    rm -rvf $FILETAR
    exit 1
  fi
  echo "$(date) - Arquivo enviado para o host $SRVHOST. Removendo o arquivo remanescente"
  rm -rvf $FILETAR.tar.gz
  echo "THE END"
}

sendFileS3(){
  YEAR="$(date +"%Y")"
  MONTH="$(date +"%m")"
  AWSS3="s3://$BUCKET/$YEAR/$MONTH/"
  echo "$(date) - Enviando o arquivo $FILETAR para o bucket remoto na s3"
  $S3CMD s3 cp $FILETAR $AWSS3
  if [ $? -ne 0 ] ; then
    echo "$(date) - Falha ao enviar o arquivo $FILETAR para o bucket remoto $BUCKET"
    exit 1
  fi
  # verificando se existe as variáveis de envio para o host remoto
  if [ -z $SRVPATH -a -z $SRVHOST ]; then
    echo "$(date) - Arquivo $FILETAR enviado com sucesso para o bucket $BUCKET, removendo o arquivo no servidor local"
    rm -rvf $FILETAR
    echo "THE END"
  else
    sendFileHost
  fi

}

backupBase
