# Script de backup de base MySQL

Parâmetros do script
  * 1º base `obrigatorio`
  * 2º "Bucket s3" `opcional`
  * 3º Servidor para transferência remota `opcional`
  * 4º Path do servidor remoto `opctional - depende so host do servidor`
  * 5º Porta de envio remoto (via scp) `opctional - depende de path e host do servidor`

Pre-requisito:
Criar um arquivo .my.cnf com os parâmetros abaixo:
```shell
[client]
user=root
host=localhost
password='senha'
socket=/var/lib/mysql/mysql.sock
```

Mudar permissões do arquivo:
```
# chmod 600 ~/.my.cnf
```

 Exemplo:
```shell
  sh script_backup.sh base_teste bucket 10.0.0.5 /home/diretorio
```
O 2º parâmetro, quando informado, aciona a função de envio para a S3 para o bucket especificado.

Do 3º parâmetro em diante, quando informado, aciona a função de envio para um host remoto, devendo informar o caminho e a porta, o usuário default que é usado é o root. 

O path de destino dos backups que vai ser utilizado é em `/mnt/backupsdodia`, se quiser modificar, basta atualizar a variável `$DEST` no script.

